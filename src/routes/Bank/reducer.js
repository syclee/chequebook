import * as chequeReducer from '../Cheque/reducer'
import * as api from '../Cheque/api';
import Immutable from 'immutable';

export const API_CLEAR_CHEQUE_SUCCESS = 'API_CLEAR_CHEQUE_SUCCESS';
export const API_BOUNCE_CHEQUE_SUCCESS = 'API_BOUNCE_CHEQUE_SUCCESS';

function getAllCheques() {
    return chequeReducer.actions.getAllCheques();
}

function clearCheque(chequeId) {
    return dispatch => {
        api.clearCheque(chequeId)
        .then(() => dispatch({ 
            type: API_CLEAR_CHEQUE_SUCCESS, 
            value: chequeId
        }));                        
    }; 
}

function bounceCheque(chequeId) {
    return dispatch => {
        api.bounceCheque(chequeId)
        .then(() => dispatch({ 
            type: API_BOUNCE_CHEQUE_SUCCESS, 
            value: chequeId
        }));                        
    };  
}

export const actions = {
    getAllCheques,
    clearCheque,
    bounceCheque
}

const ACTION_HANDLERS = {
    [chequeReducer.API_GET_ALL_CHEQUES_SUCCESS]: (state, action) => {        
        return {...state, cheques: action.value};
    },   
    [API_CLEAR_CHEQUE_SUCCESS]: (state, action) => {
        const chequeId = action.value;
        const index = state.cheques.findIndex(c => c.id == chequeId);        
        const cheque = state.cheques[index];
        var chequeList = Immutable.List(state.cheques)
                                  .set(index, {
                                      ...cheque,
                                      status: 'Cleared'
                                    });

        return {
            ...state,
            cheques: chequeList.toArray()
        };
    },
    [API_BOUNCE_CHEQUE_SUCCESS]: (state, action) => {
        const chequeId = action.value;
        const index = state.cheques.findIndex(c => c.id == chequeId);        
        const cheque = state.cheques[index];
        var chequeList = Immutable.List(state.cheques)
                                  .set(index, {
                                      ...cheque,
                                      status: 'Bounced'
                                    });

        return {
            ...state,
            cheques: chequeList.toArray()
        };
    }
};

const initialState = {
    cheques: []
};

export default function bankReducer (state=initialState, action) {
    const handler = ACTION_HANDLERS[action.type];
    return handler ? handler(state, action) : state;
}