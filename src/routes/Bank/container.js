import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actions } from './reducer';

import BankView from './components/BankView';

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

function mapStateToProps(state) {
    return {
       chequeList: state.bank.cheques,       
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(BankView);