import React from 'react';

class BankView extends React.Component {

  componentDidMount() {
    this.props.actions.getAllCheques();
  }

  amountToCurrencyStr(amountStr) {
    if(amountStr === '')
      return '';

    const n = parseFloat(amountStr);
    return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
  };

  render() {
    return (
      <div style={{textAlign: 'left'}}>
        <h1>The Bank</h1>        
        <h4>To process</h4>
        {
          this.props.chequeList.filter(c => c.status === 'Pending').map((c, i) => 
            <div key={i}>
              {c.name} - {c.date} - ${this.amountToCurrencyStr(c.amount)} <a onClick={() => this.props.actions.clearCheque(c.id)} href="#">Cleared</a> <a onClick={() => this.props.actions.bounceCheque(c.id)}  href="#">Bounced</a>
            </div>
          )             
        }
        <h4>Processed</h4>
        {
          this.props.chequeList.filter(c => c.status !== 'Pending').map((c, i) => {
            const style = {
              color: c.status === 'Bounced' ? 'red' : c.status === 'Cleared' ? 'green' : 'grey'
            };  
            return (
              <div key={i}>
                {c.name} - {c.date} - ${this.amountToCurrencyStr(c.amount)} - <b style={style}>{c.status}</b> 
              </div>
            );
          })             
        }       
      </div>);
  }
}

BankView.propTypes = {
  chequeList: React.PropTypes.array.isRequired,
  actions: React.PropTypes.object.isRequired
}

export default BankView;