import converter from 'number-to-words';

export default function moneyToReadableSentence(n) {
    const decimals = n%1;
    const dollars = n - decimals;  
    let dollarWords = '';
    let centWords = '';

    if(dollars > 0) {
        dollarWords += converter.toWords(n) + " dollars";
        centWords += " and ";
    }
                 
    if (decimals === 0)
        return dollarWords;
    
    centWords += converter.toWords(Math.round(decimals * 100)) + " cents";    
    return dollarWords + centWords;
}