import Immutable from 'immutable';
import * as helper from './helper';
import * as api from './api';

// Action Types
export const API_GET_ALL_CHEQUES_SUCCESS = 'GET_ALL_CHEQUES';
export const API_SAVE_CHEQUE_SUCCESS = 'API_SAVE_CHEQUE_SUCCESS';
export const SHOW_CHEQUE = 'SHOW_CHEQUE';

export const FORM_NAME_CHANGE = 'FORM_NAME_CHANGE';
export const FORM_DATE_CHANGE = 'FORM_DATE_CHANGE';
export const FORM_DATE_ISVALID = 'FORM_DATE_ISVALID';
export const FORM_AMOUNT_CHANGE = 'FORM_AMOUNT_CHANGE';
export const FORM_AMOUNT_ISVALID = 'FORM_AMOUNT_ISVALID';

function getAllCheques() {
    return dispatch => {
        api.getAllCheques().then(json => {
            dispatch({ type: API_GET_ALL_CHEQUES_SUCCESS, value: json });
        });                               
    };    
}

function showCheque(index) {
    return {
        type: SHOW_CHEQUE,
        value: index
    };
}

function writeCheque(name, date, amount) {     
    return dispatch => {
        api.writeCheque(name, date, amount)
        .then(() => dispatch({ 
            type: API_SAVE_CHEQUE_SUCCESS, 
            value: {name, date, amount} 
        }));                        
    }; 
}

function nameChanged(name) {
    return {
        type: FORM_NAME_CHANGE,
        value: name
    }
}

function dateChanged(date) {
    const isValidDate = helper.validateDate(date);    
    return dispatch => {
        return new Promise((resolve) => {
            dispatch({ type: FORM_DATE_CHANGE, value: date });
            resolve();
        }).then(() => {
            dispatch({ type: FORM_DATE_ISVALID, value: isValidDate });
        });        
    };         
}

function amountChanged(amount) {
    const isValidAmount = helper.validateAmount(amount);
    return dispatch => {
        return new Promise((resolve) => {
            dispatch({ type: FORM_AMOUNT_CHANGE, value: amount });
            resolve();
        }).then(() => {
            dispatch({ type: FORM_AMOUNT_ISVALID, value: isValidAmount });
        });        
    };
}

export const actions = {
  nameChanged,
  dateChanged,
  amountChanged,
  getAllCheques,
  showCheque,
  writeCheque  
}

const ACTION_HANDLERS = {
    [API_GET_ALL_CHEQUES_SUCCESS]: (state, action) => {        
        return {...state, cheques: action.value};
    },    
    [SHOW_CHEQUE]: (state, action) => {
        const index = action.value;
        const cheque = state.cheques[index];
        return {
            ...state,            
            name: cheque.name,
            date: cheque.date,
            amount: cheque.amount.toString()
        };        
    },
    [API_SAVE_CHEQUE_SUCCESS]: (state, action) => {
        const {name, date, amount} = action.value;
        const newCheque = {name, date, amount, status: 'Pending'};
        var chequeList = Immutable.List(state.cheques)
                                  .insert(0, newCheque);                
        return {
            ...state,
            name: '',
            date: '',
            amount: '',
            cheques: chequeList.toArray()
        };
    },
    [FORM_NAME_CHANGE]: (state, action) => {
        return {...state, name: action.value};
    },
    [FORM_DATE_CHANGE]: (state, action) => {
        return {...state, date: action.value};
    },
    [FORM_DATE_ISVALID]: (state, action) => {
        return {...state, isValidDate: action.value};
    },
    [FORM_AMOUNT_CHANGE]: (state, action) => {
        return {...state, amount: action.value};
    },
    [FORM_AMOUNT_ISVALID]: (state, action) => {
        return {...state, isValidAmount: action.value};
    },
};

const initialState = {
    name: '',
    date: '',
    isValidDate: true,
    amount: '',
    isValidAmount: true,
    cheques: []
};

export default function chequeReducer (state=initialState, action) {
    const handler = ACTION_HANDLERS[action.type];
    return handler ? handler(state, action) : state;
}