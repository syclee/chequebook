
const endPoint = 'http://localhost:58896/cheque/';
const headers = new Headers({
  'Content-Type': 'application/json'
});  


export function getAllCheques() {  
  const method = 'GET';  
  const request = new Request(endPoint, {
    method,    
    headers 
  });

  return fetch(request)
    .then(function(response) {    
      return response.json();
    });
}

export function writeCheque(name, date, amount) {
  const method = 'POST';
  const body = {
    name,
    date,
    amount
  };
  const request = new Request(endPoint, {
    method,
    headers,
    body: JSON.stringify(body)
  });

  return fetch(request);  
}

export function clearCheque(chequeId) {  
  const method = 'PUT';
  const body = {
    chequeId
  };
  const request = new Request(endPoint+'clear', {
    method,
    headers,
    body: JSON.stringify(body)
  });

  return fetch(request);  
}

export function bounceCheque(chequeId) {
  const method = 'PUT';
  const body = {
    chequeId
  };
  const request = new Request(endPoint+'bounce', {
    method,
    headers,
    body: JSON.stringify(body)
  });

  return fetch(request);
}