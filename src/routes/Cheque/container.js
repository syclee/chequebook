import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actions } from './reducer';

import ChequeView from './components/ChequeView';

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

function mapStateToProps(state) {
    return {
        message:       state.cheque.message,        
        name:          state.cheque.name,
        date:          state.cheque.date,
        isValidDate:   state.cheque.isValidDate,
        amount:        state.cheque.amount,
        isValidAmount: state.cheque.isValidAmount,        
        chequeList:    state.cheque.cheques
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ChequeView);