import React from 'react';
import moneyToReadableSentence from '../number_to_readable_sentence';

const TheCheque = props => {
  const amountToSentenceStr = amountStr => {
    if(amountStr === '' || !props.isValidAmount)
      return '';
    return moneyToReadableSentence(parseFloat(amountStr));
  };

  const dateToStr = (dateStr) => {
    if(dateStr === '')
      return '';

    if(!props.isValidDate)
      return 'Invalid Date';

    const dd = parseInt(dateStr.substr(0, 2));
    const mm = parseInt(dateStr.substr(3, 2)) - 1;
    const yyyy = parseInt(dateStr.substr(6));
    
    const dateObj = new Date(yyyy, mm, dd);    
    return dateObj.toDateString();
  };

  const amountToCurrencyStr = (amountStr) => {
    if(amountStr === '')
      return '';

    if(!props.isValidAmount)
      return 'Invalid Amount';

    const n = parseFloat(amountStr);
    return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
  };

  const dateStr = dateToStr(props.date);
  const amountCurrencyStr = amountToCurrencyStr(props.amount);
  const amountInSentenceStr = amountToSentenceStr(props.amount);

  return (
      <div style={{         
      position: 'relative',    
      padding: '1.5em',
      width: '50em', 
      height: '14em', 
      backgroundColor: '#faf3df', 
      margin: '0 auto', 
      WebkitBoxShadow: '2px 2px 2px 2px rgba(0,0,0,0.50)',       
    }}>
      <div style={{fontFamily: 'Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono', fontSize: '2.0em', position: 'absolute', top: '0.8em', left:'0.8em'}}>Prospa Bank</div>          
      <div style={{fontSize: '1.5em', position: 'absolute', top: '3.5em', right:'2.0em'}}>Date: <b>{dateStr}</b></div>
      <div style={{position: 'absolute', top: '3.5em', left: '1.0em', fontSize: '1.5em'}}>Name: <b>{props.name}</b></div>
      <div style={{position: 'absolute', top: '5em', right: '2.0em', fontSize: '1.5em'}}>$ <b>{amountCurrencyStr}</b></div>
      <div style={{position: 'absolute', top: '5em', left: '1.0em', fontSize: '1.5em', maxWidth: '400px', textAlign: 'left'}}>Pay to the order of: <b>{amountInSentenceStr}</b></div>
    </div>
  );
};

TheCheque.propTypes = {};

export default TheCheque;