import React from 'react';

const ChequeList = props => {      
  
  return (
    <div> 
    <p>Previously sent cheques</p>
    {
      props.list.map((c,i) => {        
        const style = {
          color: c.status === 'Bounced' ? 'red' : c.status === 'Cleared' ? 'green' : 'grey'
        }; 
        return (
          <div key={i}>
            <a onClick={() => props.showCheque(i)} href="#">{c.name} - <b style={style}>{c.status}</b></a>
          </div>
        );        
      })
    } 
    </div>
  );
};

ChequeList.propTypes = {
  list: React.PropTypes.array.isRequired,
  showCheque: React.PropTypes.func.isRequired
};

export default ChequeList;