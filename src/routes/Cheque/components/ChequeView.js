import React from 'react';
import TheCheque from './TheCheque';
import ChequeList from './ChequeList';

class ChequeView extends React.Component {
  constructor(props) {
    super(props);
    this.onNameChange = this.onNameChange.bind(this);
    this.onDateChange = this.onDateChange.bind(this);
    this.onAmountChange = this.onAmountChange.bind(this);
    this.onWriteCheque = this.onWriteCheque.bind(this);        
  }

  onNameChange(e) {
    this.props.actions.nameChanged(e.target.value);
  } 

  onDateChange(e) {
    this.props.actions.dateChanged(e.target.value);
  }

  onAmountChange(e) {
    this.props.actions.amountChanged(e.target.value);
  }

  onWriteCheque() {
    const {name, date, amount} = this.props;
    this.props.actions.writeCheque(name, date, amount);
  }

  componentDidMount() {
    this.props.actions.getAllCheques();
  }

  renderForm() {
    const {name, date, amount, isValidDate, isValidAmount} = this.props;
    const cannotClickButton = !(name && date && amount && isValidDate && isValidAmount);
    return (      
      <div style={{margin: '20px'}}>
        <div>
          <label>
            <span>Full Name</span> 
            <input type="text" value={name} onChange={this.onNameChange} className="form-control" style={{textAlign: 'center'}} />
          </label>            
        </div>
        <div>
          <label>
            <span>Date in format like 05/10/2016</span> 
            <input type="text" value={date} onChange={this.onDateChange} style={!isValidDate ? {textAlign: 'center', border: "2px solid red"} : {textAlign: 'center'}} className="form-control" />
          </label>            
        </div>
        <div>
          <label>
            <span>Amount in dollars and cents</span> 
            <input type="text" value={amount} onChange={this.onAmountChange} style={!isValidAmount ? {textAlign: 'center', border: "2px solid red"} : {textAlign: 'center'}} className="form-control" />            
          </label>            
        </div>
        <br />
        <div>    
          <button disabled={cannotClickButton} onClick={this.onWriteCheque} className="btn">Send the cheque!</button>    
        </div>
      </div>
    );
  }

  render() {
    return (
      <div>
        <h1>Chequebook</h1>
        <TheCheque name={this.props.name}
                  date={this.props.date}
                  isValidDate={this.props.isValidDate}
                  amount={this.props.amount} 
                  isValidAmount={this.props.isValidAmount}/>      
        {this.renderForm()}
        <ChequeList list={this.props.chequeList} showCheque={this.props.actions.showCheque} />                      
      </div>
    );
  }
}

ChequeView.propTypes = {      
    actions:       React.PropTypes.object,
    name:          React.PropTypes.string.isRequired,
    date:          React.PropTypes.string.isRequired,
    isValidDate:   React.PropTypes.bool.isRequired,
    amount:        React.PropTypes.string.isRequired,
    isValidAmount: React.PropTypes.bool.isRequired,
    chequeList:    React.PropTypes.array,    
};

export default ChequeView;
