export function validateDate(dateStr) {
    if (dateStr === '')
        return true;
    
    const re = /\b([0-9]{2}\/[0-9]{2}\/[0-9]{4})\b/g;
    return dateStr.match(re) !== null;
}

export function validateAmount(amountStr) {
    if(amountStr === '')
        return true;
    
    if(isNaN(amountStr))
        return false;
    
    const dotIndex = amountStr.indexOf('.');

    if(dotIndex >= 0 && 
       amountStr.substr(dotIndex+1).length > 2)
        return false;

    return true;
}