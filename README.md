# Chequebook demo

Demo built from [React Redux Starter Kit here](https://github.com/davezuko/react-redux-starter-kit) as base.

Simple demo to capture name, date and amount and displays a bank cheque.

Can view saved bank cheques, and can add new bank cheques.

This demo uses the following libraries

* [react](https://github.com/facebook/react)
* [redux](https://github.com/rackt/redux)
* [number-to-words](https://www.npmjs.com/package/number-to-words)
* [immutable-js](https://github.com/facebook/immutable-js/)

# How to run
Requirements are 

* node `^4.5.0`
* npm `^3.0.0`

Clone the repository and then

    npm install
    npm run start

Browse to http://localhost:3000/cheque