﻿using ApplicationServices.QueryModel;
using DomainModel;
using DomainModel.Commands;
using SimpleCqrs.Commanding;

namespace ApplicationServices.CommandHandlers
{
    public enum ProcessChequeStatus
    {
        AlreadyProcessed,
        Successful
    }

    public class ClearChequeCommandHandler : AggregateRootCommandHandler<ClearChequeCommand, Cheque>
    {
        private readonly ChequeList _chequeList;

        public ClearChequeCommandHandler(ChequeList chequeList)
        {
            _chequeList = chequeList;
        }

        public override int ValidateCommand(ClearChequeCommand command, Cheque cheque)
        {
            var existingChequeStatus = _chequeList.Get(command.ChequeId).Status;
            if (existingChequeStatus != ChequeStatus.Pending.ToString())
            {
                return (int) ProcessChequeStatus.AlreadyProcessed;
            }
            return (int) ProcessChequeStatus.Successful;
        }

        public override void Handle(ClearChequeCommand command, Cheque cheque)
        {
            if (ValidationResult == (int) ProcessChequeStatus.Successful)
            {
                cheque.Clear();
            }
        }
    }
}
