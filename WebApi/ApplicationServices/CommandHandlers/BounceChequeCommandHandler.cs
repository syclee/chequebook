﻿using ApplicationServices.QueryModel;
using DomainModel;
using DomainModel.Commands;
using SimpleCqrs.Commanding;

namespace ApplicationServices.CommandHandlers
{
    public class BounceChequeCommandHandler : AggregateRootCommandHandler<BounceChequeCommand, Cheque>
    {
        private readonly ChequeList _chequeList;

        public BounceChequeCommandHandler(ChequeList chequeList)
        {
            _chequeList = chequeList;
        }

        public override int ValidateCommand(BounceChequeCommand command, Cheque aggregateRoot)
        {
            var existingChequeStatus = _chequeList.Get(command.ChequeId).Status;
            if (existingChequeStatus != ChequeStatus.Pending.ToString())
            {
                return (int)ProcessChequeStatus.AlreadyProcessed;
            }
            return (int)ProcessChequeStatus.Successful;
        }

        public override void Handle(BounceChequeCommand command, Cheque cheque)
        {
            if (ValidationResult == (int) ProcessChequeStatus.Successful)
            {
                cheque.Bounce();
            }
        }
    }
}
