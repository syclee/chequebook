﻿using System;
using DomainModel;
using DomainModel.Commands;
using SimpleCqrs.Commanding;
using SimpleCqrs.Domain;

namespace ApplicationServices.CommandHandlers
{
    public enum WriteChequeStatus
    {
        Successful
    }

    public class WriteChequeCommandHandler : CommandHandler<WriteChequeCommand>
    {
        private readonly IDomainRepository _chequeRepository;

        public WriteChequeCommandHandler(IDomainRepository chequeRepository)
        {
            _chequeRepository = chequeRepository;
        }

        public override void Handle(WriteChequeCommand command)
        {
            var date = DateTime.Parse(command.Date);
            var cheque = new Cheque(command.Name, date, command.Amount);
            _chequeRepository.Save(cheque);            
        }
    }
}
