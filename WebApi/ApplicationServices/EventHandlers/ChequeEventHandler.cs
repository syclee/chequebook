﻿using System;
using System.Dynamic;
using ApplicationServices.QueryModel;
using DomainModel;
using DomainModel.Events;
using SimpleCqrs.Eventing;

namespace ApplicationServices.EventHandlers
{
    public class ChequeEventHandler : 
        IHandleDomainEvents<ChequeWrittenEvent>,
        IHandleDomainEvents<ChequeClearedEvent>,
        IHandleDomainEvents<ChequeBouncedEvent>
    {
        private readonly ChequeList _chequeList;

        public ChequeEventHandler(ChequeList chequeList)
        {
            _chequeList = chequeList;
        }

        public void Handle(ChequeWrittenEvent e)
        {
            dynamic newCheque = new ExpandoObject();
            newCheque.Id = e.ChequeId;
            newCheque.Name = e.Name;
            newCheque.Date = e.Date.ToString("dd/MM/yyyy");
            newCheque.Amount = e.Amount;
            newCheque.Status = e.Status.ToString();

            lock (_chequeList)
            {
                _chequeList.Insert(newCheque);
            }            
        }

        public void Handle(ChequeClearedEvent e)
        {
            HandleChequeStatusChange(e.ChequeId, e.Status);
        }

        public void Handle(ChequeBouncedEvent e)
        {
            HandleChequeStatusChange(e.ChequeId, e.Status);
        }

        private void HandleChequeStatusChange(Guid chequeId, ChequeStatus status)
        {
            lock (_chequeList)
            {
                var cheque = _chequeList.Get(chequeId);
                cheque.Status = status.ToString();
            }
        }
    }    
}
