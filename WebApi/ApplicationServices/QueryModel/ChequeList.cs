﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ApplicationServices.QueryModel
{
    public class ChequeList : List<dynamic>
    {
        public dynamic Get(Guid chequeId)
        {
            return this.Single(c => c.Id == chequeId);
        }

        public void Insert(dynamic newCheque)
        {
            this.Insert(0, newCheque);
        }
    }
}
