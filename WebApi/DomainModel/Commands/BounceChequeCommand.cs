﻿using System;
using SimpleCqrs.Commanding;

namespace DomainModel.Commands
{
    public class BounceChequeCommand : CommandWithAggregateRootId
    {
        public Guid ChequeId
        {
            get { return AggregateRootId; }
            set { AggregateRootId = value; }
        }
    }
}
