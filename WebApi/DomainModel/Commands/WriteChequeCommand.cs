﻿using System;
using SimpleCqrs.Commanding;

namespace DomainModel.Commands
{
    public class WriteChequeCommand : ICommand
    {
        public WriteChequeCommand(string name, string date, decimal amount)
        {
            Name = name;
            Date = date;
            Amount = amount;
        }

        public string Name { get; set; }
        public string Date { get; set; }
        public decimal Amount { get; set; }
    }
}
