﻿using System;
using DomainModel.Events;
using SimpleCqrs.Domain;

namespace DomainModel
{
    public enum ChequeStatus
    {
        Pending,
        Cleared,
        Bounced
    }

    public class Cheque : AggregateRoot
    {
        public string       Name   { get; set; }
        public DateTime     Date   { get; set; }
        public decimal      Amount { get; set; }
        public ChequeStatus Status { get; set; }

        public Cheque() { }

        public Cheque(string name, DateTime date, decimal amount)
        {
            Apply(new ChequeWrittenEvent(Guid.NewGuid(), name, date, amount));
        }

        protected void OnChequeWritten(ChequeWrittenEvent e)
        {
            Id = e.ChequeId;
            Name = e.Name;
            Date = e.Date;
            Amount = e.Amount;
            Status = e.Status;
        }

        public void Clear()
        {
            if (Status != ChequeStatus.Cleared)
            {
                Apply(new ChequeClearedEvent(Id));
            }
        }

        protected void OnChequeCleared(ChequeClearedEvent e)
        {
            Status = e.Status;
        }

        public void Bounce()
        {
            if (Status != ChequeStatus.Bounced)
            {
                Apply(new ChequeBouncedEvent(Id));
            }
        }

        protected void OnChequeBounced(ChequeBouncedEvent e)
        {
            Status = e.Status;
        }
    }
}
