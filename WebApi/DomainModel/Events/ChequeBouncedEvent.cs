﻿using System;
using SimpleCqrs.Eventing;

namespace DomainModel.Events
{
    public class ChequeBouncedEvent : DomainEvent
    {
        public Guid ChequeId
        {
            get { return AggregateRootId; }
            set { AggregateRootId = value; }
        }

        public ChequeStatus Status { get; set; }

        public ChequeBouncedEvent(Guid chequeId)
        {
            ChequeId = chequeId;
            Status = ChequeStatus.Bounced;
        }
    }
}
