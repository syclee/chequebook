﻿using System;
using SimpleCqrs.Eventing;

namespace DomainModel.Events
{
    public class ChequeClearedEvent : DomainEvent
    {
        public Guid ChequeId
        {
            get { return AggregateRootId; }
            set { AggregateRootId = value; }
        }

        public ChequeStatus Status { get; set; }

        public ChequeClearedEvent(Guid chequeId)
        {
            ChequeId = chequeId;
            Status = ChequeStatus.Cleared;
        }
    }
}
