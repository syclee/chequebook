﻿using System;
using SimpleCqrs.Eventing;

namespace DomainModel.Events
{
    public class ChequeWrittenEvent : DomainEvent
    {
        public Guid ChequeId
        {
            get { return AggregateRootId; }
            set { AggregateRootId = value; }
        }

        public string Name { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public ChequeStatus Status { get; set; }

        public ChequeWrittenEvent(Guid chequeId, string name, DateTime date, decimal amount)
        {
            ChequeId = chequeId;
            Name = name;
            Date = date;
            Amount = amount;
            Status = ChequeStatus.Pending;
        }
    }
}
