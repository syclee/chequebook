﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using DomainModel;
using DomainModel.Commands;
using Newtonsoft.Json.Serialization;
using SimpleCqrs;
using SimpleCqrs.Commanding;

namespace ChequeRest
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public static CqrsRuntime Runtime { get; set; }

        protected void Application_Start()
        {
            Runtime = new CqrsRuntime();
            Runtime.Start();

            HttpConfiguration config = GlobalConfiguration.Configuration;
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
               
            GlobalConfiguration.Configure(WebApiConfig.Register);

            InitialChequeData();
        }

        private void InitialChequeData()
        {
            // intial data
            var commandBus = ServiceLocator.Current.Resolve<ICommandBus>();
            commandBus.Send(new WriteChequeCommand("Bill Gates", "2015-10-10", (decimal) 1000000000));
            commandBus.Send(new WriteChequeCommand("Warren Buffet", "2015-11-11", (decimal)1000000000.99));
            commandBus.Send(new WriteChequeCommand("US Govt.", "2015-12-12", (decimal)1000000000000));
            commandBus.Send(new WriteChequeCommand("Chris Lee", "2016-10-15", (decimal)0.5));
            commandBus.Send(new WriteChequeCommand("Bull Grates", "2015-11-10", (decimal)1000));
        }

        protected void Application_End()
        {
            Runtime.Shutdown();
        }
    }
}
