﻿using System;
using System.IO;
using ApplicationServices.QueryModel;
using SimpleCqrs;
using SimpleCqrs.Eventing;
using SimpleCqrs.EventStore.File;
using SimpleCqrs.Unity;

namespace ChequeRest
{
    public class CqrsRuntime : SimpleCqrsRuntime<UnityServiceLocator>
    {
        protected override UnityServiceLocator GetServiceLocator()
        {
            var serviceLocator = base.GetServiceLocator();
            serviceLocator.Register(new ChequeList());
            return serviceLocator;
        }

        protected override IEventStore GetEventStore(IServiceLocator serviceLocator)
        {
            return base.GetEventStore(serviceLocator);
        }
    }
}