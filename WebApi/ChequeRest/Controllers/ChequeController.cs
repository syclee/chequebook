﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using ApplicationServices.CommandHandlers;
using ApplicationServices.QueryModel;
using DomainModel;
using DomainModel.Commands;
using SimpleCqrs;
using SimpleCqrs.Commanding;

namespace ChequeApi.Controllers
{
    [RoutePrefix("cheque")]
    [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
    public class ChequeController : ApiController
    {
        private readonly ChequeList _chequeList;
        public ICommandBus CommandBus { get; set; }

        public ChequeController() : this(ServiceLocator.Current.Resolve<ICommandBus>(), ServiceLocator.Current.Resolve<ChequeList>())
        {
        }

        public ChequeController(ICommandBus commandBus, ChequeList chequeList)
        {
            _chequeList = chequeList;
            CommandBus = commandBus;
        }

        [HttpGet]
        [Route("")]
        public HttpResponseMessage GetAll()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _chequeList);
        }

        [HttpPost]
        [Route("")]
        public WriteChequeStatus WriteCheque(WriteChequeCommand command)
        {
            return (WriteChequeStatus)CommandBus.Execute(command);            
        }

        [HttpPut]
        [Route("clear")]
        public ProcessChequeStatus ClearCheque(ClearChequeCommand command)
        {
            return (ProcessChequeStatus)CommandBus.Execute(command);            
        }

        [HttpPut]
        [Route("bounce")]
        public ProcessChequeStatus BounceCheque(BounceChequeCommand command)
        {
            return (ProcessChequeStatus)CommandBus.Execute(command);
        }
    }
}
